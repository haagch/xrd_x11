#include <iostream>
#include <stdio.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xdamage.h>

#include <xrd.h>

class XWindow;
XWindow *GetWindow(Display *dpy, Window w);

struct {
  XrdShell *xrd_shell;
  float z;
} g;
#define Z_STEP 0.1

class XWindow {
public:
  Display *_dpy;
  Window _w;
  XWindow *_next;
  XWindow *_parent;
  XWindow *_sibling;
  int _nchildren;
  XWindow *_children;
  char *_name;
  GLuint _texture;
  GulkanTexture *_gk_texture;
  int _x;
  int _y;
  int _width;
  int _height;
  int _hdepth;

  int _event_mask;

  bool _textured;
  bool _mapped;

  XWindow(Display *dpy, Window w, XWindow *next) {
    _dpy = dpy;
    _w = w;
    _next = next;
    _parent = NULL;
    _sibling = NULL;
    _nchildren = 0;
    _children = NULL;
    _name = NULL;
    _texture = false;
    _mapped = false;
    _width = 0;
    _height = 0;
  }

  void Add(XWindow *new_child) {
    XWindow *child;
    if (_children) {
      for (child = _children; child->_sibling; child = child->_sibling) {
        if (child == new_child) {
          return;
        }
      }
      if (child == new_child) {
        return;
      }
      new_child->_parent = this;
      new_child->_sibling = NULL;
      _nchildren++;
      child->_sibling = new_child;
    } else {
      new_child->_parent = this;
      new_child->_sibling = NULL;
      _nchildren++;
      _children = new_child;
    }
  }

  void UpdateHierarchy() {
    Window *children, dummy;
    unsigned int nchildren;

    if (!XQueryTree(_dpy, _w, &dummy, &dummy, &children, &nchildren)) {
      return;
    }

    _hdepth = 0;
    for (XWindow *parent = _parent; parent; parent = parent->_parent) {
      _hdepth++;
    }

    // for (int i=nchildren - 1; i >= 0; i--)
    for (int i = 0; i < nchildren; i++) {
      XWindow *child = GetWindow(_dpy, children[i]);
      if (!child) {
        continue;
      }
      Add(child);
      child->UpdateHierarchy();
    }
  }

  bool Initialize() {
    XWindowAttributes attrib;
    if (!XGetWindowAttributes(_dpy, _w, &attrib)) {
      printf(" unabled to get window attributes\n");
      return false;
    }

    XFetchName(_dpy, _w, &_name);

    _x = attrib.x;
    _y = attrib.y;
    _width = attrib.width;
    _height = attrib.height;

    _event_mask = attrib.all_event_masks;
    // attrib.override_redirect can indicate popup window!

    printf("0x%08x rect %d %d %d %d (%d) root %08x class %d, gravity bit %d "
           "win %d, backing store %d planes %d pixels %d, save under %d, map "
           "%d, events %x, do not prop %x, override %d\n",
           (int)_w, attrib.x, attrib.y, attrib.width, attrib.height,
           attrib.depth, (int)attrib.root, attrib.c_class, attrib.bit_gravity,
           attrib.win_gravity, attrib.backing_store, (int)attrib.backing_planes,
           (int)attrib.backing_pixel, attrib.save_under, attrib.map_state,
           (int)attrib.all_event_masks, (int)attrib.do_not_propagate_mask,
           attrib.override_redirect);

    if (attrib.root != _w) {
      if (attrib.c_class == InputOutput) {
        // XDamageCreate (_dpy, _w, XDamageReportRawRectangles);
      }
      XSelectInput(_dpy, _w,
                   SubstructureNotifyMask | FocusChangeMask | ExposureMask);
    }
    return true;
  }

  void Unmap() {
    if (!_mapped) {
      return;
    }
    if (_textured) {
      glDeleteTextures(1, &_texture);
      _textured = false;
    }
    _mapped = false;
    g.z -= Z_STEP;
  }

  /* returns a gulkanTexture AND updates the respective KWin::GLTexture inside
   * vrWin */
  void _allocateTexture(int width, int height) {
    G3kContext *g3k = xrd_shell_get_g3k(g.xrd_shell);
    VkImageLayout layout = g3k_context_get_upload_layout(g3k);
    GulkanClient *client = xrd_shell_get_gulkan(g.xrd_shell);
    gsize size;
    int fd;
    VkExtent2D extent = {.width = static_cast<uint32_t>(width),
                         .height = static_cast<uint32_t>(height)};
    GulkanTexture *gkTexture = gulkan_texture_new_export_fd(
        client, extent, VK_FORMAT_R8G8B8A8_SRGB, layout, &size, &fd);

    GLuint glTexId;
    glGenTextures(1, &glTexId);
    glBindTexture(GL_TEXTURE_2D, glTexId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_TILING_EXT,
                    GL_OPTIMAL_TILING_EXT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLuint glExtMemObject = 0;
    glCreateMemoryObjectsEXT(1, &glExtMemObject);
    GLint glDedicatedMem = GL_TRUE;
    glMemoryObjectParameterivEXT(glExtMemObject, GL_DEDICATED_MEMORY_OBJECT_EXT,
                                 &glDedicatedMem);
    glGetMemoryObjectParameterivEXT(
        glExtMemObject, GL_DEDICATED_MEMORY_OBJECT_EXT, &glDedicatedMem);
    glImportMemoryFdEXT(glExtMemObject, size, GL_HANDLE_TYPE_OPAQUE_FD_EXT, fd);
    glTexStorageMem2DEXT(GL_TEXTURE_2D, 1, GL_SRGB8_ALPHA8, width, height,
                         glExtMemObject, 0);
    glDeleteMemoryObjectsEXT(1, &glExtMemObject);

    if (_texture) {
      glDeleteTextures(1, &_texture);
    }

    _texture = glTexId;
    _gk_texture = gkTexture;
  }

  bool Update(int x, int y, int width, int height) {
    // GrabServer grab(_dpy);

    XWindowAttributes attrib;
    if (!XGetWindowAttributes(_dpy, _w, &attrib)) {
      printf(" unabled to get window attributes\n");
      return false;
    }

    if (_hdepth != 1) {
      printf("%08x hdepth %d\n", (int)_w, _hdepth);
      return true;
    }

    if (!_textured) {
      if (attrib.map_state == IsViewable && attrib.c_class == InputOutput) {
        x = 0;
        y = 0;
        width = attrib.width;
        height = attrib.height;
        _width = 0;
        _height = 0;

        // glGenTextures(1, &_texture);
        _allocateTexture(width, height);

        _textured = true;
      } else {
        return true;
      }
    } else {
      if (attrib.map_state != IsViewable) {
        Unmap();
        return true;
      }
    }

    if (_mapped != (attrib.map_state == IsViewable)) {
      printf("%08x mapped: %d -> %d\n", (int)_w, _mapped,
             attrib.map_state == IsViewable);
    }

    if (!_mapped && attrib.map_state == IsViewable) {
      G3kContext *g3k = xrd_shell_get_g3k(g.xrd_shell);
      const float ppm = 500;
      XrdWindow *xrdWin = xrd_window_new_from_pixels(g3k, _name, attrib.width,
                                                     attrib.height, ppm);

      g_object_set(xrdWin, "native", this, NULL);
      xrd_shell_add_window(g.xrd_shell, xrdWin, true, this);

      graphene_point3d_t point = {0, 0.5, g.z};
      g.z += Z_STEP;
      ;
      graphene_matrix_t transform;
      graphene_matrix_init_translate(&transform, &point);
      xrd_window_set_transformation(xrdWin, &transform);
      xrd_window_set_reset_transformation(xrdWin, &transform);

      xrd_window_set_and_submit_texture(xrdWin, _gk_texture);

      printf("new %dx%d window: %s\n", attrib.width, attrib.height, _name);
    }
    _mapped = attrib.map_state == IsViewable;

    glBindTexture(GL_TEXTURE_2D, _texture);
    if (_width != attrib.width || _height != attrib.height) {
      x = 0;
      y = 0;
      width = attrib.width;
      height = attrib.height;
      _width = attrib.width;
      _height = attrib.height;
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      printf("Upload %dx%d texture to gpu\n", width, height);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA,
                   GL_UNSIGNED_BYTE, 0);
    } else {
      int x2 = x + width;
      int y2 = y + height;
      // crop
      if (x > _width || y > _height || x2 <= 0 || y2 <= 0) {
        return true;
      }
      // clip
      if (x < 0) {
        width += x;
        x = 0;
      }
      if (y < 0) {
        height += y;
        y = 0;
      }
      if (x2 > _width) {
        width = _width - x;
      }
      if (y2 > _height) {
        height = _height - y;
      }
    }

    XImage *image =
        XGetImage(_dpy, _w, x, y, width, height, AllPlanes, ZPixmap);
    if (!image) {
      printf(" unabled to get the image\n");
      return false;
    }

    int bytes_per_pixel = image->bits_per_pixel / 8;
    unsigned char *texture =
        (unsigned char *)malloc(width * height * bytes_per_pixel);
    unsigned char *dst = texture;
    for (int py = 0; py < height; py++) {
      unsigned char *src =
          ((unsigned char *)image->data) + (image->bytes_per_line * py);
      for (int px = 0; px < width; px++) {
        unsigned char t;
        dst[0] = src[2];
        dst[1] = src[1];
        dst[2] = src[0];
        if (bytes_per_pixel > 3)
          dst[3] = 255;
        src += bytes_per_pixel;
        dst += bytes_per_pixel;
      }
    }

    // TODO: actually handle damage region
#if 1
    if (bytes_per_pixel == 4) {
      printf("Update 4 bpp image content\n");
      glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, GL_RGBA,
                      GL_UNSIGNED_BYTE, texture);
    } else if (bytes_per_pixel == 3) {
      printf("Update 3 bpp image content\n");
      glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, GL_RGB,
                      GL_UNSIGNED_BYTE, texture);
    } else {
      printf("depth %d\n", image->depth);
    }
#else
    if (bytes_per_pixel == 4) {
      printf("Update 4 bpp image content\n");
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGBA,
                      GL_UNSIGNED_BYTE, texture);
    } else if (bytes_per_pixel == 3) {
      printf("Update 3 bpp image content\n");
      glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGB,
                      GL_UNSIGNED_BYTE, texture);
    } else {
      printf("depth %d\n", image->depth);
    }
#endif
    free(texture);

    // XDestroyImage(image);

    return true;
  }

  int nchildren() { return _nchildren; }
  XWindow *children() { return _children; }
  bool mapped() { return _mapped; }
  Window w() { return _w; }
  XWindow *sibling() { return _sibling; }
  int x() { return _x; }
  int y() { return _y; }
};

XWindow *s_table[1024];
XWindow *GetWindow(Display *dpy, Window w) {
  int index = ((w & 0xf0000000) >> 26) | (w & 0x3f);
  XWindow *xw = s_table[index];
  while (xw && !(xw->_dpy == dpy && xw->_w == w)) {
    xw = xw->_next;
  }
  if (!xw) {
    xw = new XWindow(dpy, w, s_table[index]);
    if (!xw->Initialize()) {
      delete xw;
      return NULL;
    }
    xw->_next = s_table[index];
    s_table[index] = xw;
  }
  return xw;
}

static const char s_tabs[] =
    "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
#define TABS(x) (&s_tabs[sizeof(s_tabs) - (x)])
static void DumpWindow(Display *dpy, Window w, int depth) {
  Window *children, dummy;
  unsigned int nchildren;
  int i;
  char *window_name;
  XWindowAttributes attrib;

  if (!XFetchName(dpy, w, &window_name)) {
    window_name = NULL;
  }

  XGetWindowAttributes(dpy, w, &attrib);

  printf("%s%08x (%d, %d) \"%s\" %08x %d %d %d %d\n", TABS(depth),
         (unsigned int)w, attrib.c_class, attrib.map_state, window_name,
         (int)attrib.all_event_masks, attrib.x, attrib.y, attrib.width,
         attrib.height);

  if (!XQueryTree(dpy, w, &dummy, &dummy, &children, &nchildren)) {
    return;
  }

  for (i = 0; i < nchildren; i++) {
    DumpWindow(dpy, children[i], depth + 1);
  }

  if (children) {
    XFree((char *)children);
  }
}

static void glib_iterate() {
  // xrdesktop creates a glib main context for us, so we use that default one
  // with the NULL parameter
  while (g_main_context_pending(NULL)) {
    g_main_context_iteration(NULL, FALSE);
  }
}

int main() {
  g.xrd_shell = NULL;
  g.z = -4.5;

  Display *dpy = XOpenDisplay(NULL);
  if (!dpy) {
    printf("Failed to open DISPLAY\n");
    return 1;
  }

  if (!glfwInit()) {
    printf("Failed to init glfw\n");
    return -1;
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

  GLFWwindow *window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
  if (!window) {
    printf("Failed to create glfw window\n");
    glfwTerminate();
    return -1;
  }

  glfwMakeContextCurrent(window);

  GLenum err = glewInit();
  if (GLEW_OK != err) {
    std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
    glfwTerminate();
    return -1;
  }

  g.xrd_shell = xrd_shell_new();
  if (!g.xrd_shell) {
    printf("Failed to init xrd shell\n");
    return 1;
  }

  Window root = DefaultRootWindow(dpy);
  // DumpWindow(dpy, root, 1);

  XWindow *xw = GetWindow(dpy, root);
  xw->UpdateHierarchy();

  for (XWindow *child = xw->children(); child; child = child->sibling()) {
    child->Update(0, 0, 0, 0);
    if (child->mapped()) {
      printf("focus %08x %s\n", (int)child->w(), child->_name);
      XSetInputFocus(dpy, child->w(), RevertToParent, CurrentTime);
    }
  }

  while (1) {
    glib_iterate();

    // TODO: update like this?
    for (XWindow *child = xw->children(); child; child = child->sibling()) {
      child->Update(0, 0, 0, 0);
    }

    XEvent event;
    while (XPending(dpy) > 0) {
      XNextEvent(dpy, &event);
      switch (event.type) {
      case ConfigureNotify:
        printf("ConfigureNotify %08x\n", (int)event.xconfigure.window);
        {
          Window wabove;
          XWindow *w = GetWindow(dpy, event.xconfigure.window);
          xw->UpdateHierarchy();

          XWindow *above = NULL;
          if (XGetTransientForHint(dpy, w->w(), &wabove) && wabove != None) {
            printf("   transient for %08x\n", (int)wabove);
            above = GetWindow(dpy, wabove);
          }
          if ((!above || !above->mapped()) && event.xconfigure.above != None) {
            printf("   above %08x\n", (int)event.xconfigure.above);
            above = GetWindow(dpy, event.xconfigure.above);
          }
          if (!above)
            printf("no above\n");
          else if (!above->mapped())
            printf("above not mapped\n");

          if (above && above->mapped()) {
            float x = w->x() - above->x();
            float y = w->y() - above->y();
          }
        }
        break;
      case Expose:
        printf("Expose %08x\n", (int)event.xexpose.window);
        {
          XWindow *w = GetWindow(dpy, event.xexpose.window);
          if (w) {
            // TODO: handle damage region
            // XDamageCreate (dpy, event.xcreatewindow.window,
            // XDamageReportRawRectangles);
            w->Update(0, 0, 0, 0);
          }
        }
        break;
      case MapNotify:
        printf("Map %08x\n", (int)event.xmap.window);
        {
          XWindow *w = GetWindow(dpy, event.xmap.window);
          xw->UpdateHierarchy();
          if (w) {
            // TODO: handle damage region
            // XDamageCreate (dpy, event.xmap.window,
            // XDamageReportRawRectangles);
            w->Update(0, 0, 0, 0);
          }
        }
        break;
      case UnmapNotify:
        printf("Unmap %08x\n", (int)event.xunmap.window);
        {
          XWindow *w = GetWindow(dpy, event.xunmap.window);
          if (w) {
            w->Unmap();
            for (XWindow *child = xw->children(); child;
                 child = child->sibling()) {
              if (child->mapped()) {
                printf("focus %08x\n", (int)child->w());
                XSetInputFocus(dpy, child->w(), RevertToParent, CurrentTime);
              }
            }
          }
        }
        break;
      case FocusIn:
        printf("focus in %08x\n", (int)event.xfocus.window);
        break;
      case FocusOut:
        printf("focus out %08x\n", (int)event.xfocus.window);
        break;
      default:
        break;
#if 0
          if (event.type == damageEvent + XDamageNotify)
          {
            XDamageNotifyEvent *de = (XDamageNotifyEvent *) &event;
            XWindow * w = XDisplay::GetWindow(dpy, de->drawable);
            if (w)
            {
              /*printf ("damage %08x %08x %d %d %d %d, %d %d %d %d\n", de->drawable, w->w(),
               d e*->area.x, de->area.y, de->area.width, de->area.height,
               de->geometry.x, de->geometry.y, de->geometry.width, de->geometry.height);*/
              w->Update(de->area.x, de->area.y, de->area.width, de->area.height);
            }
          }
#endif
      }
    }
    while (XPending(dpy) > 0) {
      XNextEvent(dpy, &event);
      switch (event.type) {
      default:
        // printf("unhandled event %d\n", event.type);
        break;
      case Expose:
        break;
      case ConfigureNotify:
        // ReSizeGLScene(event.xconfigure.width, event.xconfigure.height);
        break;
      case KeyPress:
#if 0
          printf("key %08x %d \n", (int)g_kb_focus, event.xkey.keycode);
          keyPressed(event.xkey.keycode, 0, 0);
          if (g_kb_focus != None) {
            XWindow * w = XDisplay::GetWindow(dpy, g_kb_focus);
            w->SendKeyEvent(root, event.xkey.keycode, event.xkey.state, true);
          }
#endif
        break;
      case KeyRelease:
#if 0
          if (g_kb_focus != None) {
            XWindow * w = XDisplay::GetWindow(dpy, g_kb_focus);
            w->SendKeyEvent(root, event.xkey.keycode, event.xkey.state, false);
          }
#endif
        break;
#if 0
        case MotionNotify:
        {
          Vector3 ray(event.xbutton.x * 2.f / g_width - 1.f, -(event.xbutton.y * 2.f - g_height) / g_width, -1.f);
          //printf ("ray %f %f %f\n", ray._x, ray._y, ray._z);
          ray.normalize();
          XDisplay::Hit hit(g_pos * g_scale, ray);
          if (!XDisplay::HitTest(hit, PointerMotionMask))
          {
            SetMouseFocus(NULL, (int)hit._x, (int)-hit._y);
            //printf ("miss\n");
            break;
          }
          SetMouseFocus(hit._w, (int)hit._x, (int)-hit._y);
          //printf ("hit %08x %f %f\n", focus, hit._x, -hit._y);
          hit._w->SendMotionEvent(root, hit._x, -hit._y, g_button_state);
        }
        break;
        case ButtonPress:
        {
          Vector3 ray(event.xbutton.x * 2.f / g_width - 1.f, -(event.xbutton.y * 2.f - g_height) / g_width, -1.f);
          printf ("ray %f %f %f\n", ray._x, ray._y, ray._z);
          ray.normalize();
          XDisplay::Hit hit(g_pos * g_scale, ray);
          if (!XDisplay::HitTest(hit, ButtonPressMask))
          {
            printf ("miss\n");
            break;
          }
          if (hit._w->w() != g_kb_focus)
          {
            g_kb_focus = hit._w->w();
            XSetInputFocus(dpy, g_kb_focus, RevertToParent, CurrentTime);
          }
          printf ("hit %08x %f %f %f, %f %f %f\n", (int)g_kb_focus, hit._x, -hit._y, hit._t,
                  hit._matrix.translation()._x, hit._matrix.translation()._y, hit._matrix.translation()._z);
          hit._w->SendButtonEvent(root, hit._x, -hit._y, event.xbutton.button, event.xbutton.state, true);
        }
        break;
        case ButtonRelease:
        {
          Vector3 ray(event.xbutton.x * 2.f / g_width - 1.f, -(event.xbutton.y * 2.f - g_height) / g_width, -1.f);
          printf ("ray %f %f %f\n", ray._x, ray._y, ray._z);
          ray.normalize();
          XDisplay::Hit hit(g_pos * g_scale, ray);
          if (!XDisplay::HitTest(hit, ButtonReleaseMask))
          {
            printf ("miss\n");
            break;
          }
          if (hit._w->w() != g_kb_focus)
          {
            g_kb_focus = hit._w->w();
            XSetInputFocus(dpy, g_kb_focus, RevertToParent, CurrentTime);
          }
          printf ("hit %08x %f %f %f, %f %f %f\n", (int)g_kb_focus, hit._x, -hit._y, hit._t,
                  hit._matrix.translation()._x, hit._matrix.translation()._y, hit._matrix.translation()._z);
          hit._w->SendButtonEvent(root, hit._x, -hit._y, event.xbutton.button, event.xbutton.state, false);
        }
        break;
#endif
      }
    }

    // vrInputUpdate();
  }
}
